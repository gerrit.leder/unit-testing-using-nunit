﻿using System;

namespace Prime.Services
{
    public class PrimeService
    {

	private int ReverseInt(int num)
	{
	    int result=0;
	    while (num>0) 
	    {
	       result = result*10 + num%10;
	       num /= 10;
	    }
	    return result;
	}

        public bool IsPrime(int candidate)
	{
    		if (candidate < 2)
    		{
        		return false;
    		}
    		if (candidate == 2) return true;
    		if (candidate % 2 == 0)  return false;

    		var boundary = (int)Math.Floor(Math.Sqrt(candidate));

    		for (int i = 3; i <= boundary; i+=2)
    		{
    		    if (candidate % i == 0)  return false;
    		}

    		return true;       

	}

	public bool IsEmirp (int candidate)
	{	
		int reverse = ReverseInt (candidate);
		
		return IsPrime(candidate)&&IsPrime(reverse)&&candidate!=reverse;
	}

    }
}
