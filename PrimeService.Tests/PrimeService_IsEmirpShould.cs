using NUnit.Framework;
using Prime.Services;

namespace Prime.UnitTests.Services
{
    [TestFixture]
    public class PrimeService_IsEmirpShould
    {
        private readonly PrimeService _primeService;

        public PrimeService_IsEmirpShould()
        {
            _primeService = new PrimeService();
        }

        [Test]
        public void ReturnFalseGivenValueOf1()
        {
            var result = _primeService.IsEmirp(1);

            Assert.IsFalse(result, "1 should not be emirp");
        }

	[TestCase(-1)]
	[TestCase(0)]
	[TestCase(1)]
	public void ReturnFalseGivenValuesLessThan2(int value)
	{
    		var result = _primeService.IsEmirp(value);

    		Assert.IsFalse(result, $"{value} should not be emirp");
	}

	[TestCase(4)]
	[TestCase(9)]
	[TestCase(3)]
	[TestCase(11)]
	[TestCase(12)]
	[TestCase(21)]
	public void ReturnFalseGivenValuesNotEmirp(int value)
	{
    		var result = _primeService.IsEmirp(value);

    		Assert.IsFalse(result, $"{value} should not be emirp");
	}


	[TestCase(13)]
	[TestCase(31)]
	[TestCase(17)]
	[TestCase(71)]
	public void ReturnTrueGivenValuesEmirp(int value)
	{
    		var result = _primeService.IsEmirp(value);

    		Assert.IsTrue(result, $"{value} should be emirp");
	}


    }
}
